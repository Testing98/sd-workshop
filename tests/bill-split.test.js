const BillSplitter = require('../src/bill-split.js');

test('Splitting 200 between 2 to equal 100', () => {
    expect(BillSplitter.split(200, 2)).toBe(100);
});

test('Splitting 111 between 2 to equal 56', () => {
    expect(BillSplitter.split(111, 2)).toBe(56);
});

test('NEgative currency should not be allowed',()=>{
    expect(()=>{
        BillSplitter.split(-100,2);
    }).toThrow();
});